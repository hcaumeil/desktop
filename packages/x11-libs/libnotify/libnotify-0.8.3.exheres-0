# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ]
require meson

SUMMARY="Notification Interface Library"
HOMEPAGE="https://gnome.org/"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="gobject-introspection gtk-doc"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-ns-stylesheets
        dev-libs/libxslt
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gi-docgen[>=2021.7] )
    build+run:
        x11-libs/gdk-pixbuf:2.0[gobject-introspection?]
        dev-libs/glib:2[>=2.62.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.12] )
    build+test:
        x11-libs/gtk+:3[>=2.90]
    post:
        virtual/notification-daemon
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dman=true
    -Ddocbook_docs=disabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

src_prepare() {
    meson_src_prepare

    # Fix install location of [gtk-doc]
    edo sed -e "s/libnotify-@0@'.format(api_version_major)/${PNVR}'/" \
        -i docs/reference/meson.build
}

