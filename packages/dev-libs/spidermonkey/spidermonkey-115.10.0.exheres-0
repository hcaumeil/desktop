# Copyright 2009 Richard Brown <rbrown@exherbo.org>
# Copyright 2011 Mike Kazantsev <mk.fraggod@gmail.com>
# Copyright 2012 Wouter van Kesteren <woutershep@gmail.com>
# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017 Marc-Antoine Perennou <keruspe@exherno.org>
# Distributed under the terms of the GNU General Public License v2

require spidermonkey toolchain-funcs providers

DOWNLOADS="https://ftp.mozilla.org/pub/firefox/releases/${PV}esr/source/firefox-${PV}esr.source.tar.xz"

LICENCES="|| ( MPL-1.1 MPL-2.0 GPL-2 GPL-3 LGPL-2.1 LGPL-3 )"
SLOT="$(ever major)"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    lto [[ description = [ Enable cross-language (c++ + rust) link-time optimisations ] ]]
"

DEPENDENCIES="
    build:
        app-arch/zip
        dev-lang/clang:*
        dev-lang/yasm [[ note = [ for icu ] ]]
        dev-lang/python:*[>=3.6][sqlite]
        dev-lang/rust:*[>=1.47.0]
        virtual/pkg-config
        lto? ( sys-devel/lld:* )
    build+run:
        dev-libs/icu:=[>=73.1]
        dev-libs/libffi:=[>=3.1]
        dev-libs/nspr[>=4.32]
        sys-libs/readline:=
        sys-libs/zlib
"

WORK="${WORKBASE}"/firefox-${PV}/js/src

pkg_setup() {
    # spidermonkey build system requires that SHELL is always set.
    # It's missing sometimes in chroot environments, so force it here.
    export SHELL=/bin/sh
}

src_prepare() {
    default

    edo cd "${WORKBASE}/firefox-${PV}"
    # Those checks use hg/git
    for check in vanilla_allocations js_msg_encoding macroassembler_style spidermonkey_style; do
        echo "" > config/check_${check}.py
    done

    expatch "${FILES}"/${SLOT}/*

    edo cd js/src
    export M4=m4
    export AWK=awk
    export AC_MACRODIR="${WORKBASE}/firefox-${PV}/build/autoconf/"
}

src_configure() {
    if ! exhost --is-native -q ; then
        export HOST_CC="$(exhost --build)-cc"
        export TARGET_CC="${CC}"
    fi

    # Cross-language lto requires building C/C++ components with clang, and linking with lld
    optionq lto && providers_set 'cpp clang' 'cc clang' 'c++ clang' 'ld lld'

    # jemalloc causes segv
    edo ./configure                          \
        --host=$(exhost --build)             \
        --target=$(exhost --target)          \
        --prefix=/usr/$(exhost --target)     \
        --libdir=/usr/$(exhost --target)/lib \
        --datadir=/usr/share                 \
        --enable-optimize                    \
        --enable-readline                    \
        --enable-shared-js                   \
        --enable-ui-locale=en_US             \
        --disable-debug                      \
        --disable-jemalloc                   \
        --disable-strip                      \
        --with-intl-api                      \
        --with-system-icu                    \
        --with-system-nspr                   \
        --with-system-zlib                   \
        --with-toolchain-prefix=$(exhost --tool-prefix) \
        $(option lto && echo "--enable-lto=cross") \
        $(cc-is-clang || echo --with-clang-path=/usr/host/bin/$(exhost --tool-prefix)clang)
}

src_install() {
    default
    edo rm "${IMAGE}"usr/$(exhost --target)/lib/libjs_static.ajs
}

src_test() {
    esandbox allow_net "unix:${TEMP%/}/pymp-*/listener-*"
    default
    esandbox disallow_net "unix:${TEMP%/}/pymp-*/listener-*"
}

