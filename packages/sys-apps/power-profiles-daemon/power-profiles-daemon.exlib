# Copyright 2021 Florentin Dubois <florentin.dubois@hey.com>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix="https://gitlab.freedesktop.org" user="upower" suffix="tar.bz2" new_download_scheme=true ] \
    python [ blacklist=2 multibuild=false has_lib=false has_test=true ] \
    test-dbus-daemon meson \
    systemd-service

export_exlib_phases src_test

SUMMARY="Makes power profiles handling available over D-Bus"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        dev-python/shtab[python_abis:*(-)?]
        gnome-desktop/libgudev[>=234]
        sys-apps/upower
        sys-auth/polkit:1[>=0.99]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    test:
        dev-libs/umockdev
        dev-python/dbus-python[python_abis:*(-)?]
        dev-python/python-dbusmock[python_abis:*(-)?]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
        sys-fs/e2fsprogs
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbashcomp=disabled
    -Dgtk_doc=false
    -Dmanpage=disabled
    -Dpylint=disabled
    -Dzshcomp=''
)
MESON_SRC_CONFIGURE_OPTIONS=(
    "providers:systemd -Dsystemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}"
    "providers:eudev -Dsystemdsystemunitdir="
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

power-profiles-daemon_src_test() {
    esandbox allow_net "unix:${TEMP%/}"

    # python-dbusmock templates
    export XDG_DATA_DIRS="/usr/$(exhost --target)/lib/python$(python_get_abi)/site-packages/dbusmock/templates"

    test-dbus-daemon_run-tests meson_src_test

    esandbox disallow_net "unix:${TEMP%/}"
}

