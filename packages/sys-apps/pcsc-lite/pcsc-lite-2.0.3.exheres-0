# Copyright 2009-2017 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pcsc-lite-1.5.3.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require option-renames [ renames=[ 'policykit polkit' ] ]
require s6-rc-service

SUMMARY="PC/SC Architecture smartcard middleware library"
DESCRIPTION="Middleware to access a smart card using SCard API (PC/SC)."
HOMEPAGE="https://pcsclite.apdu.fr/"
DOWNLOADS="${HOMEPAGE}files/${PNV}.tar.bz2"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    polkit [[ description = [ Allow pcscd to run as separate user ] ]]
    static
    (
        libusb [[ description = [ Use libusb support instead of udev ] ]]
        providers: eudev systemd
    ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:* [[ note = [ pod2man ] ]]
        sys-devel/flex
        virtual/pkg-config
    build+run:
        libusb? ( dev-libs/libusb:1 )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        polkit? ( sys-auth/polkit:1[>=0.111] )
    run:
        group/pcscd
        user/pcscd
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-serial
    # Pulls in -Werror if enabled
    --disable-strict
    --enable-documentation
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'polkit'
    'static'
    'libusb'
    '!libusb libudev'
    'providers:systemd libsystemd'
)

src_install() {
    default

    dodoc AUTHORS HELP README SECURITY ChangeLog

    keepdir /etc/reader.conf.d
    keepdir /usr/$(exhost --target)/lib/pcsc/drivers

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/pcscd 0755 pcscd pcscd
EOF
    install_s6-rc_files
}

