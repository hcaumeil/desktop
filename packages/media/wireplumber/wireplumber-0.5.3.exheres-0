# Copyright 2020 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix="https://gitlab.freedesktop.org" user=pipewire suffix=tar.bz2 new_download_scheme=true ]
require lua [ whitelist="5.3 5.4" multibuild=false ]
require meson [ cross_prefix=true ]
require systemd-service
require test-dbus-daemon
require zsh-completion

SUMMARY="Modular session and policy manager for PipeWire"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    doc
    gobject-introspection
    ( providers: elogind systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/cpptoml
        sys-devel/gettext
        doc? (
            app-doc/doxygen[>=1.8.0][dot]
            dev-python/breathe
            dev-python/Sphinx[>=2.1.0]
            dev-python/sphinx_rtd_theme
        )
        gobject-introspection? (
            app-doc/doxygen[>=1.8.0]
            dev-python/lxml
            gnome-desktop/gobject-introspection:1
        )
    build+run:
        dev-libs/glib:2[>=2.68.0]
        media/pipewire[>=1.0.2]
        providers:elogind? ( sys-auth/elogind )
        providers:systemd? ( sys-apps/systemd )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddaemon=true
    -Dmodules=true
    -Dsystem-lua=true
    -Dsystemd-system-unit-dir=${SYSTEMDSYSTEMUNITDIR}
    -Dsystemd-user-unit-dir=${SYSTEMDUSERUNITDIR}
    -Dtools=true
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    doc
    'gobject-introspection introspection'
    'providers:elogind elogind'
    'providers:systemd systemd'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'providers:systemd systemd-system-service'
    'providers:systemd systemd-user-service'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Ddbus-tests=true -Ddbus-tests=false'
    '-Dtests=true -Dtests=false'
)

src_prepare() {
    meson_src_prepare

    # meson and missing docdir...
    edo sed -e "/wireplumber_doc_dir = /s/'${PN}'/'${PNVR}'/" \
        -i meson.build
}

src_configure() {
    meson_src_configure \
        -Dsystem-lua-version=$(lua_get_abi)
}

src_install() {
    meson_src_install

    option zsh-completion || edo rm -r "${IMAGE}"/usr/share/zsh
}

src_test() {
    esandbox allow_net "unix:/tmp/wp-test-server-*"
    esandbox allow_net --connect "unix:/run/systemd/userdb/io.systemd.DynamicUser"

    test-dbus-daemon_run-tests meson_src_test

    esandbox disallow_net "unix:/tmp/wp-test-server-*"
    esandbox disallow_net --connect "unix:/run/systemd/userdb/io.systemd.DynamicUser"
}

