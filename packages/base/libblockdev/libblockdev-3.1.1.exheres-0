# Copyright 2017-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=storaged-project release=${PV}-1 suffix=tar.gz ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="A library for low-level manipulation with block devices"
DESCRIPTION="
libblockdev is a C library supporting GObject introspection for manipulation of block devices. It
has a plugin-based architecture where each technology (like LVM, Btrfs, MD RAID, Swap,...) is
implemented in a separate plugin, possibly with multiple implementations (e.g. using LVM CLI or the
new LVM DBus API).
"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    btrfs [[ description = [ Support for Btrfs filesystems ] ]]
    cryptsetup [[ description = [ Support for LUKS encrypted volumes ] ]]
    gobject-introspection
    gtk-doc
    lvm [[ description = [ Support for LVM volumes ] ]]
    mdraid [[ description = [ Support for software RAID (MD-RAID) ] ]]
    mpath [[ description = [ Support for multipath devices ] ]]
    nvme [[ description = [ Support for querying information about NVMe devices ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/glib:2[>=2.42.2]
        dev-libs/libbytesize[>=0.1]
        sys-apps/kmod[>=19]
        sys-apps/util-linux[>=2.30.0] [[ note = [ uuid, libblkid, libmount ] ]]
        sys-fs/e2fsprogs
        cryptsetup? (
            app-crypt/volume_key
            dev-libs/nss[>=3.18.0]
            sys-apps/keyutils
            sys-fs/cryptsetup[>=2.3.0]
        )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.3.0] )
        lvm? (
            sys-fs/lvm2[>=1.02.93]
            sys-fs/parted[>=3.2]
        )
        mpath? ( sys-fs/lvm2[>=1.02.93] )
        nvme? ( sys-libs/libnvme[>=1.3] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd[>=216] )
    run:
        sys-apps/gptfdisk[>=0.8.6]
        btrfs? ( sys-fs/btrfs-progs[>=3.18.2] )
        mdraid? ( sys-fs/mdadm[>=3.3.2] )
        mpath? ( sys-fs/multipath-tools )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/b7517843882b9bf2ea5665f3706c0e6b52a3fdde.patch
    "${FILES}"/ee5cf5ab9126356e2a48ba6c5ced54486485b625.patch
    "${FILES}"/527687d813c1deb69f1948b18aac117494bfeec6.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-escrow
    --with-fs
    --with-loop
    --with-part
    --with-swap
    --without-dm
    --without-nvdimm
    --without-python3
    --without-s390
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
   'gobject-introspection introspection'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    btrfs
    'cryptsetup crypto'
    gtk-doc
    lvm
    'lvm lvm_dbus'
    'lvm tools'
    mdraid
    mpath
    nvme
)
DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-tests --disable-tests' )

